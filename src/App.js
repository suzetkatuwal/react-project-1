import React, { Component } from 'react';
import './Assets.js';
import Search from './component/search/Search.js';
import Main from './component/main/Main.js';

class App extends Component {

  API_KEY = 'AIzaSyBLdTHuaJPWGLCqIYeklUuW-NS71ELEoVA';
  constructor(props) {
    super(props);
    this.state = {
      list: [],
      mainVideo: {},
      searchItem: 'Movies',
      apiKey: this.API_KEY
    };

    this.searchItem = this.searchItem.bind(this);
  }

  componentDidMount() {
    
  }
 
  
  searchItem(videos) {
    this.setState({ list: videos });
    if (this.state.list.length) {
      this.setState({
        mainVideo: this.state.list[0]
      })
    }


  }



  render() {
    return (
      <div className="MainClass">

        <Search data={this.state} onHeaderSelect={this.searchItem} />
        
        <Main data={this.state} />
        

      </div>
    );
  }
}

export default App;
