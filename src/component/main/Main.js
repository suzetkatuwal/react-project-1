import React, { Component } from 'react';

class Main extends Component {
    constructor(props) {
        super(props);

        this.state = props.data;
        this.listLoop = this.listLoop.bind(this);
        
    }

    static getDerivedStateFromProps(props, state) {
        // console.log(props.data.mainVideo);
        // console.log(state.mainVideo);

        // if(props.data.mainVideo.etag !== state.mainVideo.etag){
        //     return{
        //         list : props.data.list,
        //         mainVideo: props.data.mainVideo
        //     }
        // }else{
        //     return null;
        // }
    }
      


    getTitle() {
        if (this.state.mainVideo.snippet) {
          return this.state.mainVideo.snippet.title;
        } else {
          return '';
        }
      }
    
      getVideoUrl() {
    
        if (this.state.mainVideo.id) {
          return 'https://www.youtube.com/embed/' + this.state.mainVideo.id.videoId;
        } else {
          return '';
        }
      }
    
      getDescription() {
        if (this.state.mainVideo.snippet) {
          return 'https://www.youtube.com/embed/' + this.state.mainVideo.snippet.description;
        } else {
          return '';
        }
      }
    
      handleClick(event, data) {
        this.setState({
          mainVideo: data
        })
      }
    
      listLoop(data) {
        return (
          <a key={data.id.videoId} onClick={(e) => this.handleClick(e, data)} value={data} className="list-group-item list-group-item-action flex-column align-items-start">
            <img src={data.snippet.thumbnails.high.url} className="img-thumbnail" />
            <div className="d-flex w-100 justify-content-between">
    
              <h5 className="mb-1">{data.snippet.title}</h5>
    
            </div>
            <p className="mb-1">{data.snippet.description}</p>
            <small>{data.snippet.channelTitle}</small>
          </a>
        )
      }
    
      

    render() {
        return (
            <div className="container-fluid">
                <div className="row">
                    <div className="col-8">
                        <div className="card">
                            <div className="card-header">
                                {this.getTitle()}
                            </div>
                            <div className="card-body">
                                <iframe title={this.getTitle()} width="100%" height="480"
                                    src={this.getVideoUrl()}>
                                </iframe>
                                <h5 className="card-title">{this.getTitle()}</h5>
                                <p className="card-text">{this.getDescription()}</p>
                            </div>
                        </div>
                    </div>
                    <div className="col-4">
                        <div className="list-group">
                            {this.state.list.map(this.listLoop)}
                        </div>
                    </div>
                </div>
            </div>
        )
    }

}

export default Main;