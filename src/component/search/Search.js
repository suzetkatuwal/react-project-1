import React, {Component} from 'react';
import searchYouTube from 'youtube-api-search';

class Search extends Component{
    constructor(props){
        super(props);
        
        this.state = props.data;

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    

    componentDidMount() {
        searchYouTube({ key: this.state.apiKey, term: this.state.searchItem }, (videos) => {
            
            this.props.onHeaderSelect(videos);
     
        })
      }

    handleChange(event) {
        this.setState({searchItem: event.target.value});
      }
    
    handleSubmit(event) {
        event.preventDefault();
        searchYouTube({ key: this.state.apiKey, term: this.state.searchItem }, (videos) => {

            this.props.onHeaderSelect(videos);

        
          this.setState({
            searchItem: ''
          })

        })
    }


    render(){

        return(

            <div className="header">
                <nav className="navbar navbar-dark bg-dark">
                    <form className="form-inline col-12"  onSubmit={this.handleSubmit}>
                    <input className="form-control mr-sm-2 col-10 " value={this.state.searchItem} onChange={this.handleChange} type="search" placeholder="Search Video..." />
                    <button className="btn btn-outline-success my-2 my-sm-0  col-1"  type="submit">Search</button>
                    </form>
                </nav>
            </div>

        )

    }

    

}

export default Search;